<html lang="en-US">
<head>
    <meta charset="UTF-8" />
    <title>Ánima Ventures</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description" />
    <meta name="keywords" content="Palabras claves" />
    <meta name="author" content="YUSMEL GAMBOA BENITEZ" />
    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
    <link rel="manifest" href="/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="./css/style.css"/>


</head>
<body>
	<div class="container-fluid">

        <header class="row">
            <nav class="navbar navbar-toggleable-md navbar-light">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand hidden-md-up" href="/"><img src="img/logo.png" width="35" title="Ánima Ventures"></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <div class="logo hidden-sm-down ">
                        <a href="/"><img class="hidden-md-down" src="img/logo.png" width="80" title="Ánima Ventures"></a>
                    </div>
                    <div class="menu">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#inicio">Ventures <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#somos">somos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#empresas">empresas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#quien">quién</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#quiero">quiero</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

            <div class="main row pd-0">
                <div id="inicio" class="sections act">
                    <!--section class="creamos">
                        <p class="text-center encabezado">¨Creamos empresas que solucionan problemas¨</p>
                    </section-->
                    <section class="home">
                            <div class="foto">
                                <img class="img-fluid d-block mx-auto" src="img/home.gif">
                            </div>
                    </section>
                </div>
                <div id="somos" class="sections">
                    <section id="somo">
                        <div class="somos">
                            <p>Somos una <b>Company Builder.</b></p>
                            <p>Tenemos la oportunidad de</p>
                            <p> crear negocios que tengan </p>
                            <p><b>impacto positivo</b>, porque el</p>
                            <p>argumento de "esas cosas no</p>
                            <p> generan dinero" ya no es válido.</p>
                        </div>
                    </section>
                </div>
                <div id="empresas" class="sections s">
                    <div class="factory">
                        <div class="col-12 pd-0 empresas">
                            <div class="row">

                                <div class="col-3 em pd-0">
                                    <img src="img/microwd.png" data-id="microwd" width="100">
                                    <div class="col-12 microwd-mv">
                                        <div class="col-12 mx-auto">

                                            <a href="https://microwd.es" target="_blank"><h1 class="text-center">Microwd.</h1></a>
                                        </div>
                                        <div class="col-12 mx-auto mt-1 text-center">
                                            <p class="mt-3">Es una plataforma que conecta mujeres emprendedoras en Latinoamérica con ahorradores en
                                                España que buscan <span>rentabilidad económica e impacto social.</span></p>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-3 em pd-0">
                                    <img src="img/tuuulibreria.png" data-id="tuuulibreria" width="100">
                                    <div class="col-12 tuuulibreria-mv">
                                        <div class="col-12 mx-auto">

                                            <a href="http://tuuulibreria.org" target="_blank"><h1 class="text-center">Tuuulibrería.</h1></a>
                                        </div>
                                        <div class="col-12 mx-auto mt-1 text-center">
                                            <p class="mt-3">Es la primera librería donde tu decides lo que pagas por los libros. Tiene como objetivo <span>hacer
                                        accesible la lectura a todo el mundo,</span> independientemente de sus recursos o ingresos.</p>

                                        </div>
                                    </div>
                                </div>






                                <div class="col-3 em pd-0">
                                    <img src="img/anima.png" data-id="anima" width="100">

                                    <div class="col-12 mx-auto mt-3 anima-mv">
                                        <div class="col-12 mx-auto">
                                            <a href="http://animasoluciones.es" target="_blank"><h1 class="text-center">Ánima Soluciones.</h1></a>
                                        </div>
                                        <div class="col-12 mx-auto mt-1 kd-le">
                                            <p class="mt-3 text-center">No es un proveedor. Es tu equipo de marketing, programación, diseño, comunicación y finanzas. <span>Ya que ha muerto la consultoría, que viva la consultoría.</span></p>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-3 em pd-0">
                                    <img src="img/kiddo.png" data-id="kiddo" width="100">
                                    <div class="col-12 mx-auto mt-3 kd-mv">
                                        <div class="col-12 mx-auto">
                                            <a href="http://kiddobytes.com"><h1 class="text-center">Kiddobytes.</h1></a>
                                        </div>
                                        <div class="col-12 mx-auto mt-1 kd-le">
                                            <p class=" text-center mt-3">Es un espacio de aprendizaje diferente, un puente entre la enseñanza y la diversión. Un lugar
                                                basado en <span>la creatividad y la enseñanza transversal</span> donde se aprende jugando y mirando al
                                                futuro.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!--div class="col-3 em pd-0">
                                    <img src="img/anima.png" data-id="anima" width="100">

                                    <div class="col-12 mx-auto mt-3 anima-mv">
                                        <div class="col-12 mx-auto">
                                            <a href="http://animasoluciones.es" target="_blank"><h1 class="text-center">Ánima Press.</h1></a>
                                        </div>
                                        <div class="col-12 mx-auto mt-1 kd-le">
                                            <p class="mt-3 text-center">Es un espacio de aprendizaje diferente, un puente entre la enseñanza y la diversión. Un lugar
                                                basado en <span>la creatividad y la enseñanza transversal</span> donde se aprende jugando y mirando al
                                                futuro.
                                            </p>
                                        </div>
                                    </div>

                                </div-->



                            </div>
                        </div>

                        <div id="microwd" class="col-12 mc-hi-mv ver">
                            <div class="col-12 mx-auto">

                                <a href="https://microwd.es" target="_blank"><h1>Microwd.</h1></a>
                            </div>
                            <div class="col-12 mx-auto mt-1">
                                <p class="mt-3">Es una plataforma que conecta mujeres emprendedoras en Latinoamérica con ahorradores en
                                    España que buscan <span>rentabilidad económica e impacto social.</span></p>
                            </div>
                        </div>
                        <div id="tuuulibreria" class="col-12 tu-hi-mv">
                            <div class="col-12 mx-auto">

                                <a href="http://tuuulibreria.org" target="_blank"><h1>Tuuulibrería.</h1></a>
                            </div>
                            <div class="col-12 mx-auto mt-1">
                                <p class="mt-3">Es la primera librería donde tu decides lo que pagas por los libros. Tiene como objetivo <span>hacer
                                        accesible la lectura a todo el mundo,</span> independientemente de sus recursos o ingresos.</p>

                            </div>
                        </div>

                        <div id="anima" class="col-12 anima-hi-mv">
                            <div class="col-12 mx-auto">

                                <a href="http://animasoluciones.es" target="_blank"><h1>Ánima Soluciones.</h1></a>
                            </div>
                            <div class="col-12 mx-auto mt-1">
                                <p class="mt-3">No es un proveedor. Es tu equipo de marketing, programación, diseño, comunicación y finanzas. <span>Ya que ha muerto la consultoría, que viva la consultoría.</span></p>
                            </div>
                        </div>

                        <div id="kiddo" class="col-12 kd-hi-m">
                            <div class="col-12 mx-auto">
                                <a href="http://kiddobytes.com" target="_blank"><h1>Kiddobytes.</h1></a>
                            </div>
                            <div class="col-12 mx-auto mt-1 kd-le">
                                <p class="mt-3">Es un espacio de aprendizaje diferente, un puente entre la enseñanza y la diversión. Un lugar
                                    basado en <span>la creatividad y la enseñanza transversal</span> donde se aprende jugando y mirando al
                                    futuro.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="quien" class="sections ">
                    <div class="factory" id="quien">
                        <div class="row mv-show">
                            <div class="col-3">
                                <img class="img-fluid" src="img/alemovil.png">
                            </div>
                            <div class="col-9">
                                <p class="name"><span>Alejandro</span> de León <br/>Moreno.</p>
                                <p>Montamos empresas que siguen dos principios:</p>
                                <ul class="mt-3 mb-3">
                                    <li><span>Bottom-up</span>: Que sean las personas las que decidan, qué, cómo y cuánto.</li>
                                    <li><span>Rentabilidad económica e impacto social,</span> siempre ambas.</li>
                                </ul>
                                <p>Hemos creado: Microwd, Tuuulibrería, Ánima soluciones, Kiddobytes, Ánima Ventures.</p>
                                <p class="mt-3">Y seguimos creando.</p>
                            </div>

                        </div>

                        <div class="quien-pic mx-auto mv-quiero">
                            <img class="img-fluid" src="img/ale.png">
                        </div>
                        <div class="quien-text container mx-auto oculto mv-quiero">
                            <div class="text-ale">
                                <p class="name"><span>Alejandro</span> de León <br/>Moreno.</p>
                                <p>Montamos empresas que siguen dos principios:</p>
                                <ul class="mt-3 mb-3">
                                    <li><span>Bottom-up</span>: Que sean las personas las que decidan, qué, cómo y cuánto.</li>
                                    <li><span>Rentabilidad económica e impacto social,</span> siempre ambas.</li>
                                </ul>
                                <p>Hemos creado: Microwd, Tuuulibrería, Ánima soluciones, Kiddobytes, Ánima Press.</p>
                                <p class="mt-3">Y seguimos creando.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="quiero" class="sections">
                    <section class="creamos">
                        <p class="text-center encabezado">¿Quieres formar parte de Ánima?</p>
                    </section>
                    <section class="home">
                        <div class="foto">
                            <a href="mailto:alejandro@animaventures.com"><img class="img-fluid d-block mx-auto" src="img/si.gif"></a>
                        </div>
                    </section>
                </div>
            </div>
            <!--div class="footer pd-0">
                <div class="row">
                    <div class="col-4 date">
                        <p class="text-center">Ánima Ventures <?php echo Date('Y');?></p>
                    </div>
                    <div class="col-8">
                        <ul class="redes">
                            <li>Twitter</li>
                            <li>Mail</li>
                        </ul>
                    </div>
                </div>
            </div-->
            <footer class="row">
            	<div class="col-8 col-sm-4 col-md-4 date">
                        <p class="text-center">Ánima Ventures <?php echo Date('Y');?><br/><span class="dir">Calle Puenteáreas 18.<br> Madrid 28002<br/>(+34) 912 98 11 07</span></p>
                    </div>
                    <div class="col-4 col-sm-8 col-md-8">
                        <ul class="redes mt-md-4">
                            <li><a href="mailto:alejandro@animaventures.com"><img class="img-fluid" width="30" src="img/email.png"></a> </li>
                        </ul>
                    </div>
            </footer>
    </div>
    <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="./js/popper.min.js"></script>
    <script type="text/javascript" src="./js/tether.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/font-awsome.js"></script>
    <script type="text/javascript" src="./js/main.js"></script>
</body>
</html>
